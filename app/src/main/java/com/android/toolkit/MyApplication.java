package com.android.toolkit;

import android.app.Application;

import com.android.utils.UtilsManager;
import com.android.utils.log.Logger;

/**
 * @Introduce: .
 * @Author: lvww
 * @Date: 2023/10/11
 * @Description:
 */
public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        UtilsManager.init(this);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        Logger.d("Current memory is low");
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        UtilsManager.clearCache();
    }
}
