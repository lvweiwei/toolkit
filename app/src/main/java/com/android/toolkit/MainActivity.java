package com.android.toolkit;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.android.utils.ConvertUtils;
import com.android.utils.log.Logger;

import java.util.ArrayList;

/**
 * @Introduce: .
 * @Author: lvww
 * @Date: 2023/10/11
 * @Description:
 */
public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    public void onClickLogPrint(View view) {
        Logger.d("测试无Tag log");
    }

    public void onClickLogPrintTag(View view) {
        Logger.d(TAG, "测试有Tag log");
    }

    public void log_print_for(View view) {
        for (int i = 0; i < 100; i++) {
            Logger.d( "测试循环 log 100次 current index : " + i);
            Logger.d(TAG, "测试循环 log 100次", "current index : " + i);
        }
    }

    public void log_print_run(View view) {
        for (int i = 0; i < 100; i++) {
            new Thread(() -> {
                Logger.d("测试高并发log");
                Logger.d(TAG, "测试高并发log");
            }).start();
        }
    }

    public void convertToInt(View view) {
        Logger.d(TAG, ConvertUtils.str2Int(""));
        Logger.d(TAG, ConvertUtils.str2Int("2"));
        Logger.d(TAG, ConvertUtils.str2Int("", 1));
        Logger.d(TAG, ConvertUtils.str2Int("2", 1));

        Logger.d(TAG, ConvertUtils.double2int(null));
        Logger.d(TAG, ConvertUtils.double2int(2.12));
        Logger.d(TAG, ConvertUtils.double2int(2.59, 0));

        Logger.d(TAG, ConvertUtils.float2int(null));
        Logger.d(TAG, ConvertUtils.float2int(1.12f));
        Logger.d(TAG, ConvertUtils.float2int(1.59f, 0));

        Logger.d(TAG, ConvertUtils.ln2int(null));
        Logger.d(TAG, ConvertUtils.ln2int(123l));
        Logger.d(TAG, ConvertUtils.ln2int(321l, 0));
    }

    public void convertToStr(View view) {
        Logger.d(TAG, ConvertUtils.convertString(null));
        Logger.d(TAG, ConvertUtils.convertString(""));
        Logger.d(TAG, ConvertUtils.convertString("123456"));
        Logger.d(TAG, ConvertUtils.convertString(new Object()));
        Logger.d(TAG, ConvertUtils.convertString(new ArrayList<String>()));
    }

    public void convertToLong(View view) {
        Logger.d(TAG, ConvertUtils.int2ln(null));
        Logger.d(TAG, ConvertUtils.int2ln(121));
        Logger.d(TAG, ConvertUtils.int2ln(null, 12));
        Logger.d(TAG, ConvertUtils.int2ln(356, 0));

        Logger.d(TAG, ConvertUtils.str2Long(null));
        Logger.d(TAG, ConvertUtils.str2Long("123"));
        Logger.d(TAG, ConvertUtils.str2Long("圣诞节哦"));
    }

    public void convertToDouble(View view) {
        Logger.d(TAG, ConvertUtils.str2Float(null));
        Logger.d(TAG, ConvertUtils.str2Float("0.11"));
        Logger.d(TAG, ConvertUtils.str2Float("哈哈哈"));

        Logger.d(TAG, ConvertUtils.str2Double(null));
        Logger.d(TAG, ConvertUtils.str2Double("1.11sdasd"));
        Logger.d(TAG, ConvertUtils.str2Double("1.11564654"));
        Logger.d(TAG, ConvertUtils.str2Double("dwadawd dsad"));
    }
}