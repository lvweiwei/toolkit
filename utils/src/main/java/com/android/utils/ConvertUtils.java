package com.android.utils;

import androidx.annotation.Nullable;

import com.android.utils.log.Logger;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @Introduce: 对象比较及转换工具类.
 * @Author: lvww
 * @Date: 2023/10/11
 * @Description:
 */
public class ConvertUtils {

    public static String convertString(Object object) {
        return convertString(object, null);
    }

    public static Integer str2Int(@Nullable String src) {
        return str2Int(src, 0);
    }

    public static Integer str2Int(@Nullable String src, int defaultValue) {
        return str2Int(src, 10, defaultValue);
    }

    public static Long str2Long(@Nullable String src) {
        return str2Long(src, 0);
    }

    public static Long str2Long(@Nullable String src, long defaultValue) {
        return str2Long(src, 10, defaultValue);
    }

    public static Float str2Float(@Nullable String src) {
        return str2Float(src, 0);
    }

    public static Double str2Double(@Nullable String src) {
        return str2Double(src, 0);
    }

    public static Long int2ln(Integer arg) {
        return int2ln(arg, 0);
    }

    public static Long float2ln(Float arg) {
        return float2ln(arg, 0);
    }

    public static Long double2ln(Double arg) {
        return double2ln(arg, 0);
    }

    public static Integer ln2int(Long arg) {
        return ln2int(arg, 0);
    }

    public static Integer float2int(Float args) {
        return float2int(args, 0);
    }

    public static Integer double2int(Double args) {
        return double2int(args, 0);
    }

    /**
     * 转换字符串.
     *
     * @param object 任意类型
     * @return str value
     */
    public static String convertString(@Nullable Object object, String defaultValue) {
        try {
            assert !isNull(object);
            if (object instanceof String) return object.toString();
            return String.valueOf(object);
        } catch (AssertionError | ClassCastException exception) {
            Logger.e(exception.toString());
            return defaultValue;
        }
    }

    /**
     * 字符串转换整型.
     *
     * @param src          需要转换的字符
     * @param radix        进制类型
     * @param defaultValue 转换失败的默认值
     * @return int value
     */
    public static Integer str2Int(@Nullable String src, int radix, int defaultValue) {
        try {
            assert !isEmpty(src);
            return Integer.valueOf(src, radix);
        } catch (AssertionError | NumberFormatException throwable) {
            if (throwable instanceof AssertionError)
                Logger.e(throwable.toString());
            return defaultValue;
        }
    }

    /**
     * 字符串转换长整型.
     *
     * @param src          需要转换的字符
     * @param radix        进制类型
     * @param defaultValue 转换失败的默认值
     * @return long value
     */
    public static Long str2Long(@Nullable String src, int radix, long defaultValue) {
        try {
            assert !isEmpty(src);
            return Long.valueOf(src, radix);
        } catch (AssertionError | NumberFormatException throwable) {
            Logger.e(throwable.toString());
            return defaultValue;
        }
    }

    /**
     * 字符串转换浮点类型.
     *
     * @param src          需要转换的字符
     * @param defaultValue 转换失败的默认值
     * @return float value
     */
    public static Float str2Float(@Nullable String src, float defaultValue) {
        try {
            assert !isEmpty(src);
            return Float.valueOf(src);
        } catch (AssertionError | NumberFormatException throwable) {
            Logger.e(throwable.toString());
            return defaultValue;
        }
    }

    /**
     * 字符串转换双精度类型.
     *
     * @param src          需要转换的字符
     * @param defaultValue 转换失败的默认值
     * @return double value
     */
    public static Double str2Double(@Nullable String src, double defaultValue) {
        try {
            assert !isEmpty(src);
            return Double.parseDouble(src);
        } catch (AssertionError | NumberFormatException throwable) {
            Logger.e(throwable.toString());
            return defaultValue;
        }
    }

    /**
     * Integer To Long.
     *
     * @param arg          要转换的值
     * @param defaultValue default value
     * @return Long value
     */
    public static Long int2ln(@Nullable Integer arg, long defaultValue) {
        try {
            assert !isNull(arg);
            BigDecimal bd = new BigDecimal(arg);
            return bd.longValue();
        } catch (AssertionError error) {
            Logger.e(error.toString());
            return defaultValue;
        }
    }

    /**
     * Float to Long.
     *
     * @param arg          float type
     * @param defaultValue default value
     * @return Long type
     */
    public static Long float2ln(@Nullable Float arg, long defaultValue) {
        try {
            assert !isNull(arg);
            BigDecimal bd = new BigDecimal(arg);
            return bd.longValue();
        } catch (AssertionError | NumberFormatException error) {
            Logger.e(error.toString());
            return defaultValue;
        }
    }

    /**
     * Double to Long.
     *
     * @param arg          double type
     * @param defaultValue default value
     * @return Long type
     */
    public static Long double2ln(@Nullable Double arg, long defaultValue) {
        try {
            assert !isNull(arg);
            BigDecimal bd = new BigDecimal(arg);
            return bd.longValue();
        } catch (AssertionError | NumberFormatException error) {
            Logger.e(error.toString());
            return defaultValue;
        }
    }


    /**
     * Long To Integer.
     *
     * @param arg          要转换的值
     * @param defaultValue default value
     * @return Integer value
     */
    public static Integer ln2int(@Nullable Long arg, int defaultValue) {
        try {
            assert !isNull(arg);
            BigDecimal bd = new BigDecimal(arg);
            return bd.intValue();
        } catch (AssertionError error) {
            Logger.e(error.toString());
            return defaultValue;
        }
    }

    /**
     * 浮点转换int
     *
     * @param args         float
     * @param defaultValue default value
     * @return Integer value
     */
    public static Integer float2int(Float args, int defaultValue) {
        try {
            assert !isNull(args);
            return Math.round(args);
        } catch (AssertionError error) {
            Logger.e(error.toString());
            return defaultValue;
        }
    }

    /**
     * 双精度转换int
     *
     * @param args         double
     * @param defaultValue default value
     * @return Integer value
     */
    public static Integer double2int(Double args, int defaultValue) {
        try {
            assert !isNull(args);
            return ln2int(Math.round(args), defaultValue);
        } catch (AssertionError error) {
            Logger.e(error.toString());
            return defaultValue;
        }
    }

    /**
     * 字符串比较（包括大小写）.
     *
     * @param args  this args
     * @param args1 this args1
     * @return true/false
     */
    public static boolean equals(String args, String args1) {
        if (isEmpty(args)) return isEmpty(args1);
        return !isEmpty(args1) && args.equals(args1);
    }

    /**
     * 对象比较.
     *
     * @param args  this args
     * @param args1 this args1
     * @return true/false
     */
    public static boolean equals(Object args, Object args1) {
        if (isNull(args)) return isNull(args1);
        return !isNull(args1) && Objects.equals(args, args1);
    }

    /**
     * Long类型比较
     *
     * @param args
     * @param args1
     * @return true/false
     */
    public static boolean equals(Long args, Long args1) {
        if (isNull(args)) return isNull(args1);
        return 0 == args.compareTo(args1);
    }

    /**
     * Integer类型比较
     *
     * @param args
     * @param args1
     * @return true/false
     */
    public static boolean equals(Integer args, Integer args1) {
        if (isNull(args)) return isNull(args1);
        return 0 == args.compareTo(args1);
    }

    /**
     * Double类型比较
     *
     * @param args
     * @param args1
     * @return true/false
     */
    public static boolean equals(Double args, Double args1) {
        if (isNull(args)) return isNull(args1);
        return 0 == args.compareTo(args1);
    }

    /**
     * Float类型比较
     *
     * @param args
     * @param args1
     * @return true/false
     */
    public static boolean equals(Float args, Float args1) {
        if (isNull(args)) return isNull(args1);
        return 0 == args.compareTo(args1);
    }

    /**
     * 字符串比较（忽略大小写）.
     *
     * @param args
     * @param args1
     * @return true/false
     */
    public static boolean equalsIgnoreCase(String args, String args1) {
        return !args.equalsIgnoreCase(args1);
    }

    /**
     * 字符穿判空.
     *
     * @param str current str
     * @return true/false
     */
    public static boolean isNull(String str) {
        return null == str;
    }

    /**
     * 字符穿判空（包含长度判断）.
     *
     * @param str current str
     * @return true/false
     */
    public static boolean isEmpty(String str) {
        return null == str || str.isEmpty();
    }

    /**
     * 对象判空.
     *
     * @param obj
     * @return true/false
     */
    public static boolean isNull(Object obj) {
        return Objects.isNull(obj);
    }

    /**
     * 对象判空.
     *
     * @param obj
     * @return <T>obj
     */
    public static <T> T isNullRequire(T obj) {
        return Objects.requireNonNull(obj, "T obj is null");
    }

    /**
     * 对象判空.
     *
     * @param obj
     * @return true/false
     */
    public static boolean isEmpty(Object obj) {
        if (obj instanceof String)
            return isEmpty(convertString(obj));
        return isNull(obj);
    }


    /**
     * 数组只判空不判断长度.
     *
     * @param objects object 数组
     * @return true/false
     */
    public static <T> boolean isNull(T[] objects) {
        return null == objects;
    }

    /**
     * 数组判空.
     *
     * @param objects object 数组
     * @return true/false
     */
    public static <T> boolean isEmpty(T[] objects) {
        return null == objects || 0 == objects.length;
    }

    /**
     * 数组判空(byte不支持泛型不知道为什么).
     *
     * @param objects byte 数组
     * @return true/false
     */
    public static boolean isEmpty(byte[] objects) {
        return null == objects || 0 == objects.length;
    }

    /**
     * List集合只判空不判断长度.
     *
     * @param objects object 数组
     * @return true/false
     */
    public static boolean isNull(List<?> objects) {
        return null == objects;
    }

    /**
     * List集合判空.
     *
     * @param objects object 数组
     * @return true/false
     */
    public static boolean isEmpty(List<?> objects) {
        return null == objects || objects.isEmpty();
    }

    /**
     * Map集合判空只判断对象不判断长度.
     *
     * @param map map集合
     * @param <T> map key type
     * @param <R> map value type
     * @return true/false
     */
    public static <T, R> boolean isNull(Map<T, R> map) {
        return null == map;
    }

    /**
     * Map集合判空.
     *
     * @param map map集合
     * @param <T> map key type
     * @param <R> map value type
     * @return true/false
     */
    public static <T, R> boolean isEmpty(Map<T, R> map) {
        return isNull(map) || map.isEmpty();
    }

    /**
     * 比较大小 Long类型
     *
     * @param arg
     * @param args
     * @return 0: arg = args、1: arg > args、-1：arg < args
     */
    public static int compare(Long arg, Long args) {
        try {
            assert (!isNull(arg) && !isNull(args));
            return arg.compareTo(args);
        } catch (AssertionError error) {
            throw new RuntimeException("Argument is null");
        }
    }

    /**
     * 比较大小 Long类型
     *
     * @param arg
     * @param args
     * @return true: arg > args
     */
    public static boolean compareGreater(Long arg, Long args) {
        try {
            assert (!isNull(arg) && !isNull(args));
            return 0 < arg.compareTo(args);
        } catch (AssertionError error) {
            throw new RuntimeException("Argument is null");
        }
    }

    /**
     * 比较大小 Integer类型
     *
     * @param arg
     * @param args
     * @return 0: arg = args、1: arg > args、-1：arg < args
     */
    public static int compare(Integer arg, Integer args) {
        try {
            assert (!isNull(arg) && !isNull(args));
            return arg.compareTo(args);
        } catch (AssertionError error) {
            throw new RuntimeException("Argument is null");
        }
    }

    /**
     * 比较大小 Integer类型
     *
     * @param arg
     * @param args
     * @return true: arg > args
     */
    public static boolean compareGreater(Integer arg, Integer args) {
        try {
            assert (!isNull(arg) && !isNull(args));
            return 0 < arg.compareTo(args);
        } catch (AssertionError error) {
            throw new RuntimeException("Argument is null");
        }
    }

    /**
     * 比较大小 Double类型
     *
     * @param arg
     * @param args
     * @return 0: arg = args、1: arg > args、-1：arg < args
     */
    public static int compare(Double arg, Double args) {
        try {
            assert (!isNull(arg) && !isNull(args));
            return arg.compareTo(args);
        } catch (AssertionError error) {
            throw new RuntimeException("Argument is null");
        }
    }

    /**
     * 比较大小 Double类型
     *
     * @param arg
     * @param args
     * @return true: arg > args
     */
    public static boolean compareGreater(Double arg, Double args) {
        try {
            assert (!isNull(arg) && !isNull(args));
            return 0 < arg.compareTo(args);
        } catch (AssertionError error) {
            throw new RuntimeException("Argument is null");
        }
    }

    /**
     * 比较大小 Float类型
     *
     * @param arg
     * @param args
     * @return 0: arg = args、1: arg > args、-1：arg < args
     */
    public static int compare(Float arg, Float args) {
        try {
            assert (!isNull(arg) && !isNull(args));
            return arg.compareTo(args);
        } catch (AssertionError error) {
            throw new RuntimeException("Argument is null");
        }
    }

    /**
     * 比较大小 Float类型
     *
     * @param arg
     * @param args
     * @return true: arg > args
     */
    public static boolean compareGreater(Float arg, Float args) {
        try {
            assert (!isNull(arg) && !isNull(args));
            return 0 < arg.compareTo(args);
        } catch (AssertionError error) {
            throw new RuntimeException("Argument is null");
        }
    }

    /**
     * 比较对象大小.
     *
     * @param obj1 要比较的对象
     * @param obj2 要比较的对象
     * @param c    比较器，一般用于指明对比属性
     * @param <T>
     * @return left > right 返回1 , left < right 返回-1 , left = right 返回0.
     */
    public static <T> int compare(T obj1, T obj2, Comparator<? super T> c) {
        return Objects.compare(obj1, obj2, c);
    }

    /**
     * 计算数值差.
     *
     * @param arg
     * @param args
     * @return
     */
    public static long numberDiff(Long arg, Long args) {
        try {
            assert (!isNull(arg) && !isNull(args));
            return Math.abs(arg - args);
        } catch (AssertionError error) {
            throw new RuntimeException("Argument is null");
        }
    }

    /**
     * 计算数值差.
     *
     * @param arg
     * @param args
     * @return
     */
    public static int numberDiff(Integer arg, Integer args) {
        try {
            assert (!isNull(arg) && !isNull(args));
            return Math.abs(arg - args);
        } catch (AssertionError error) {
            throw new RuntimeException("Argument is null");
        }
    }

    /**
     * 计算数值差.
     *
     * @param arg
     * @param args
     * @return
     */
    public static double numberDiff(Double arg, Double args) {
        try {
            assert (!isNull(arg) && !isNull(args));
            return Math.abs(arg - args);
        } catch (AssertionError error) {
            throw new RuntimeException("Argument is null");
        }
    }

    /**
     * 计算数值差.
     *
     * @param arg
     * @param args
     * @return
     */
    public static float numberDiff(Float arg, Float args) {
        try {
            assert (!isNull(arg) && !isNull(args));
            return Math.abs(arg - args);
        } catch (AssertionError error) {
            throw new RuntimeException("Argument is null");
        }
    }

    /**
     * 校验参数.
     *
     * @param method 方法名字
     * @param args   校验的参数
     * @return
     */
    public static void checkParam(String method, Object... args) {
        if (isEmpty(args)) throw new RuntimeException(method + " 参数异常可能为空，无法继续执行");
        int size = args.length;
        for (int i = 0; i < args.length; i++) {
            if (isEmpty(args[i]))
                throw new RuntimeException(method + " 第" + (i + 1) + "参数异常，无法继续执行");
        }
    }
}