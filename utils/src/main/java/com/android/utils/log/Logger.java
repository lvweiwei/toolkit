package com.android.utils.log;

/**
 * @Introduce: API.
 * @Author: lvww
 * @Date: 2023/10/11
 * @Description:
 */
public class Logger {

    public static void v(String args) {
        LogUtils.verbose(args);
    }

    public static void v(String tag, Object... args) {
        LogUtils.verbose(tag, args);
    }

    public static void d(String args) {
        LogUtils.debug(args);
    }

    public static void d(String tag, Object... args) {
        LogUtils.debug(tag, args);
    }

    public static void i(String args) {
        LogUtils.info(args);
    }

    public static void i(String tag, Object... args) {
        LogUtils.info(tag, args);
    }

    public static void w(String args) {
        LogUtils.warn(args);
    }

    public static void w(String tag, Object... args) {
        LogUtils.warn(tag, args);
    }

    public static void e(String args) {
        LogUtils.error(args);
    }

    public static void e(String tag, Object... args) {
        LogUtils.error(tag, args);
    }

    public static void switchLog(boolean logSwitch) {
        LogUtils.switchLog(logSwitch);
    }

    public static void setLogLevel(int level) {
        LogUtils.setLogLevel(level);
    }

    public static void setDefaultTag(String defaultTag) {
        LogUtils.setDefaultTag(defaultTag);
    }

    public static void initLogUtils(boolean logSwitch, int level) {
        LogUtils.initLogUtils(logSwitch, level);
    }

    public static final String getDefaultTag() {
        return LogUtils.getDefaultTagName();
    }
}
